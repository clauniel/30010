
#ifndef _TIMEKEEPING_H
#define _TIMEKEEPING_H

void setup_timer( char timer, unsigned long u_period, char priority, void (*callback)() );

#endif
