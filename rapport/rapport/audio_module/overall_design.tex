\subsection{Audio module design}

As described under the analysis, the audio module has to be capable of both
generating sounds and playing predefined wave-files. The processor is chosen to
run in real-time with a frequency of 44kHz, this is done to reduce aliasing
artifacts - in theory this is enough to allow an output spanning the full
audible range, given sufficient filtering and taking care not to generating any
tones above the nyquist-frequency; we did however not have the time to dive
fully into this theory and thus some aliasing will occur.

The design of the audio module will be done like a digital model of a modular
synthesizer, taking inspiration from eg. the free-to-try software
\emph{SynthEdit0} in which different block (oscillators, filters, transient
generators, wave-file players etc.) can be connected to form whole synthesizers.
See figure:~\ref{fig:synthedit}.

\stdfig{0.8}{audiomodule/synthedit.jpg}{The design of the synthesizers took
inspiration from the block approach as seen in eg. the free program
\emph{Synth Edit} \cite{SynthEdit}. Image from \cite{SEimg}}{fig:synthedit}

This approach allows for both easy programming of the modules, as this becomes
an highly manageable problem; easy programming of the synthesizers, this is just
connecting the blocks; and provides an easily expandable and reusable library.
This also allows for easy programming reusable container modules, eg. a
standard synthesizer.

Communication is done over SPI, using MIDI (\emph{Musical Instrument Digital
Interface}) commands. This allows for a MIDI interpreter module, working in
similar ways as all other modules.

\subsubsection{Overview}
We decided that the background music should be the 80'es classic
\emph{Popcorn}.
For this we need three voices, lead, bass and percussion.
The score is stored in 6 arrays (notes and gate/trigger for each voice) and a
sequencer goes through this array, giving notes and triggers to the synthesisers
and wave-player. This is illustrated in figure:~\ref{fig:audioflow}

\stdfig{0.8}{audiomodule/audioflow}{Block diagram of the audio
module}{fig:audioflow}

The two red blocks, \emph{raw Synth} and \emph{filter Synth} are the
synthesisers for the lead voice and the bass voice respectively. These modules
are application specific.

For the lead voice we create a module called `\emph{RawSynth}, because this only
contains the bare minimums for a synth: An oscillator with a wave-shaper and a
\emph{AHDSR} module, controlling the volume. The wave shape is set to a stepped
saw-wave. See figure:~\ref{fig:rawsynth} for a block diagram.

\stdfig{0.8}{audiomodule/rawsynth}{Block diagram of the RawSynth used for the
lead voice}{fig:rawsynth}

For the bass voice we choose a synth with a 2-pole resonant state variable
filter, using the low-pass, doing a downward sweep for a synthy \emph{``dauww''}
sound. a block diagram for this can be seen in figure:~\ref{fig:filtersynth}.
This module we name \emph{FilterSynth}, a square wave is chosen for the
wave table, as this gives a nice tone for the bass (the fundamental is strong
in a square wave, compared to eg. saw wave)

\stdfig{0.8}{audiomodule/filtersynth}{Block diagram of the FilterSynth used for
the bass voice}{fig:filtersynth}

\subsubsection{Design of the core modules}
The following core modules were designed:
\begin{itemize}
  \item VCO: A basic oscillator with an input for the note to play and a saw
  wave output, no bandwidth limiting.
  \item LFO: A basic low frequency oscillator with a saw wave output and a
  \emph{tick} output, a delta function once every cycle.
  \item Sequencer: A sequencer, takes an array and turns it into a sequence -
  outputting each element in the array one at a time. The sequencer advances
  when the tick input is high and resets when the reset is high. The sequencer
  also has an output producing a delta function when looping around.
  \item LowPass: A simple 1-pole \emph{IIR} low-pass filter. Has an input for
  the signal and filter coefficient. This module was not used in the final product.
  \item SVF: A 2-pole resonant state variable filter. Has inputs for signal,
  filter coefficient, damping coefficient and outputs for hi-pass, band-pass and
  low-pass.
  \item AHDSR: \emph{Attack Hold Decay Sustain Release} module as described in
  section \ref{synthxp}. Has inputs for trigger, gate and the 5 parameters. only
  has one output.
  \item WaveShaper: A module capable of turning a saw wave from the VCO into any
  wave-shape - provided this wave-shape is given in an array. Can also perform
  bandwidth-limited wave-shaping if the supplied array supports it.
  \item MidiInterpreter: A module capable of reading from a FIFO (\emph{First
  In First Out}, as opposed to a stack) buffer and interpreting these signals.
  Has outputs for note: the last note played; trigger: high if the note was
  played in the current frame and playing: indicating if the music should be
  playing or not. Ideally there would be a ``note on'' and ``trigger'' array for
  every note (127 notes) on every midi channel (8 channels) - but this was not
  optimal for the given processor.
  \item OneShotter: A module for playing back small wave-files. It can
  re-trigger and takes a pointer to an array, storing the wave-file and the
  length of said file as arguments when updating.
\end{itemize}

\subsubsection{Design of the FIFO buffer}
A general utility structure \emph{FIFOBuffer} is designed, this module has the
following functions:
\begin{itemize}
  \item push: Pushes a data point into the beginning of the FIFO, returns 1 on
  success and 0 if the buffer is full.
  \item peak: Get the value of a data point a specified length from the end of
  the FIFO. Returns 0 if no data point exists here and 1 if it does.
  \item pop: Pop a data element from the end of the FIFO. Returns 1 if there
  was an element to pop and 0 if the FIFO is empty.
\end{itemize}
