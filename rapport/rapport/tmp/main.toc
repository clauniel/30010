\select@language {english}
\contentsline {section}{\numberline {1}Who did what}{4}{section.1}
\contentsline {section}{\numberline {2}Introduction}{7}{section.2}
\contentsline {section}{\numberline {3}Specification}{8}{section.3}
\contentsline {section}{\numberline {4}Analysis}{9}{section.4}
\contentsline {section}{\numberline {5}Design}{10}{section.5}
\contentsline {subsection}{\numberline {5.1}Game Design}{10}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Software Architecture Design}{10}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}API and Hardware modules}{14}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Hardware abstraction modules}{14}{subsubsection.5.3.1}
\contentsline {paragraph}{SPI module}{14}{section*.5}
\contentsline {paragraph}{Timerlib}{14}{section*.6}
\contentsline {paragraph}{ADC input}{14}{section*.7}
\contentsline {subsubsection}{\numberline {5.3.2}API modules}{14}{subsubsection.5.3.2}
\contentsline {paragraph}{Input module}{14}{section*.8}
\contentsline {paragraph}{Math}{14}{section*.9}
\contentsline {paragraph}{Sound}{14}{section*.10}
\contentsline {paragraph}{Graphics}{14}{section*.11}
\contentsline {paragraph}{Timekeeping}{15}{section*.12}
\contentsline {section}{\numberline {6}Implementation of Game Software}{16}{section.6}
\contentsline {subsection}{\numberline {6.1}Hardware Abstraction Layer Implementation}{16}{subsection.6.1}
\contentsline {paragraph}{SPI module}{16}{section*.13}
\contentsline {paragraph}{Timerlib}{16}{section*.14}
\contentsline {paragraph}{ADC input}{16}{section*.15}
\contentsline {subsection}{\numberline {6.2}API Layer Implementation}{16}{subsection.6.2}
\contentsline {paragraph}{Input Module}{16}{section*.16}
\contentsline {paragraph}{Sound Module}{16}{section*.17}
\contentsline {paragraph}{Math module}{16}{section*.18}
\contentsline {paragraph}{Timekeeping Module}{16}{section*.19}
\contentsline {paragraph}{Graphics Module}{17}{section*.20}
\contentsline {subsection}{\numberline {6.3}Application Layer Implementation}{17}{subsection.6.3}
\contentsline {paragraph}{Main function}{17}{section*.21}
\contentsline {paragraph}{Levels Module}{17}{section*.22}
\contentsline {paragraph}{Backgrounds module}{17}{section*.23}
\contentsline {paragraph}{Entities}{17}{section*.24}
\contentsline {paragraph}{Ball Entity}{17}{section*.25}
\contentsline {paragraph}{Striker Entity}{18}{section*.27}
\contentsline {paragraph}{Brick Entity}{18}{section*.28}
\contentsline {subsection}{\numberline {6.4}Game Testing}{19}{subsection.6.4}
\contentsline {section}{\numberline {7}The audio module}{20}{section.7}
\contentsline {subsection}{\numberline {7.1}Audio module introduction}{21}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}An introduction to subtractive synthesis}{21}{subsubsection.7.1.1}
\contentsline {subsection}{\numberline {7.2}Audio module analysis}{22}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Audio module design}{23}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}Overview}{23}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}Design of the core modules}{24}{subsubsection.7.3.2}
\contentsline {subsubsection}{\numberline {7.3.3}Design of the FIFO buffer}{25}{subsubsection.7.3.3}
\contentsline {subsection}{\numberline {7.4}Audio module implementation}{26}{subsection.7.4}
\contentsline {subsubsection}{\numberline {7.4.1}FIFO buffer}{26}{subsubsection.7.4.1}
\contentsline {subsubsection}{\numberline {7.4.2}Hardware Interface Layer (\emph {HIL})}{27}{subsubsection.7.4.2}
\contentsline {paragraph}{Timing LED}{28}{section*.36}
\contentsline {subsubsection}{\numberline {7.4.3}implementation of the synth modules}{28}{subsubsection.7.4.3}
\contentsline {paragraph}{VCO}{28}{section*.37}
\contentsline {paragraph}{LFO}{28}{section*.38}
\contentsline {paragraph}{Sequencer}{28}{section*.39}
\contentsline {paragraph}{LowPass}{28}{section*.40}
\contentsline {paragraph}{SVF}{28}{section*.41}
\contentsline {paragraph}{AHDSR}{28}{section*.42}
\contentsline {paragraph}{WaveShaper}{28}{section*.43}
\contentsline {paragraph}{MidiInterpreter}{29}{section*.44}
\contentsline {paragraph}{OneShotter}{29}{section*.45}
\contentsline {subsection}{\numberline {7.5}Implementation of main function}{29}{subsection.7.5}
\contentsline {subsection}{\numberline {7.6}Testing the audio module}{31}{subsection.7.6}
\contentsline {subsection}{\numberline {7.7}Audio module results}{31}{subsection.7.7}
\contentsline {section}{\numberline {8}The resulting game}{32}{section.8}
\contentsline {section}{\numberline {9}Discussion}{34}{section.9}
\contentsline {section}{\numberline {10}Conclusion}{35}{section.10}
\contentsline {section}{\numberline {A}Manual}{37}{appendix.A}
\contentsline {subsection}{\numberline {A.1}SMASHING BRICKS!}{37}{subsection.A.1}
\contentsline {subsubsection}{\numberline {A.1.1}Setting up the Nigel entertainment System}{37}{subsubsection.A.1.1}
\contentsline {subsubsection}{\numberline {A.1.2}Setting up the display:}{37}{subsubsection.A.1.2}
\contentsline {subsubsection}{\numberline {A.1.3}Setting up sliding input}{37}{subsubsection.A.1.3}
\contentsline {subsubsection}{\numberline {A.1.4}Setting up the external audio noisemaker}{37}{subsubsection.A.1.4}
\contentsline {subsection}{\numberline {A.2}You are now ready to play!}{37}{subsection.A.2}
\contentsline {subsubsection}{\numberline {A.2.1}How to play}{37}{subsubsection.A.2.1}
\contentsline {subsubsection}{\numberline {A.2.2}Loosing the game}{37}{subsubsection.A.2.2}
\contentsline {subsubsection}{\numberline {A.2.3}Winning the game}{38}{subsubsection.A.2.3}
\contentsline {section}{\numberline {B}Game module source code}{38}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Application layer}{38}{subsection.B.1}
\contentsline {subsection}{\numberline {B.2}API layer}{51}{subsection.B.2}
\contentsline {subsection}{\numberline {B.3}Hardware interface layer}{60}{subsection.B.3}
\contentsline {section}{\numberline {C}Audio module source code}{62}{appendix.C}
\contentsline {subsection}{\numberline {C.1}Application}{62}{subsection.C.1}
\contentsline {subsection}{\numberline {C.2}FIFO}{67}{subsection.C.2}
\contentsline {subsection}{\numberline {C.3}synth\_lib}{69}{subsection.C.3}
\contentsline {subsection}{\numberline {C.4}Hardware, for \emph {Windows}}{78}{subsection.C.4}
\contentsline {subsection}{\numberline {C.5}Hardware, for \emph {Stellaris Launchpad}}{81}{subsection.C.5}
\contentsline {section}{\numberline {D}Java utility tools}{89}{appendix.D}
\contentsline {subsection}{\numberline {D.1}Tool for converting bitmaps to array of characters and freground/background terminal colors}{89}{subsection.D.1}
\contentsline {subsection}{\numberline {D.2}Tool for converting text file with score to array of sequencer data}{92}{subsection.D.2}
\contentsline {subsection}{\numberline {D.3}Tool for converting 16bit signed \emph {.WAV} files to unsigned 8bit arrays}{94}{subsection.D.3}
\contentsline {section}{\numberline {E}Journal}{107}{appendix.E}
\contentsline {subsection}{\numberline {E.1}Exercise 1 and 2 }{107}{subsection.E.1}
\contentsline {subsection}{\numberline {E.2}Exercise 3 and 4 }{111}{subsection.E.2}
\contentsline {subsection}{\numberline {E.3}Exercise 5 }{115}{subsection.E.3}
\contentsline {subsection}{\numberline {E.4}Exercise 6 }{116}{subsection.E.4}
\contentsline {subsection}{\numberline {E.5}Exercise 7 and 8 }{120}{subsection.E.5}
