#ifndef _SOUND_H
#define _SOUND_H


void setup_sound();
void play_sound( unsigned char sound );
void toggle_track( unsigned char toggle );

#endif
